#!/bin/bash

purge() {
	echo "Purging..."
	docker-compose down
	docker volume rm nginx-proxy-conf nginx-proxy-vhost nginx-proxy-html nginx-proxy-certs ldap-config ldap-database
	rm -rf /srv/docker-data/nginx-proxy
	rm -rf /srv/docker-data/ldap
	exit
}

while true
do
	read -r -p "Purge will remove also external volume data. Are you Sure? To only stop container, use stop.sh [yes/no] " input

	case $input in
		[yY][eE][sS])
		purge
		;;

		[nN][oO])
		echo "Bye"
		exit
		;;

		*)
		echo "Pleas enter 'yes' or 'no'"
		;;
	esac
done