set -x

mkdir -p /srv/docker-data/nginx-proxy/conf
mkdir -p /srv/docker-data/nginx-proxy/vhost
mkdir -p /srv/docker-data/nginx-proxy/html
mkdir -p /srv/docker-data/nginx-proxy/certs
mkdir -p /srv/docker-data/ldap/config
mkdir -p /srv/docker-data/ldap/database

sudo docker volume create --name nginx-proxy-conf \
 --opt type=none \
 --opt device=/srv/docker-data/nginx-proxy/conf \
 --opt o=bind
sudo docker volume create --name nginx-proxy-vhost \
 --opt type=none \
 --opt device=/srv/docker-data/nginx-proxy/vhost \
 --opt o=bind
sudo docker volume create --name nginx-proxy-html \
 --opt type=none \
 --opt device=/srv/docker-data/nginx-proxy/html \
 --opt o=bind
sudo docker volume create --name nginx-proxy-certs \
 --opt type=none \
 --opt device=/srv/docker-data/nginx-proxy/certs \
 --opt o=bind
sudo docker volume create --name ldap-config \
 --opt type=none \
 --opt device=/srv/docker-data/ldap/config \
 --opt o=bind
sudo docker volume create --name ldap-database \
 --opt type=none \
 --opt device=/srv/docker-data/ldap/database \
 --opt o=bind

docker-compose up --no-start